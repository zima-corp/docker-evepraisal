#!/bin/sh -e

if [ "$#" -eq 0 ]; then
	set -- evepraisal
fi

# allow the container to be started with `--user`
if [ "$1" = 'evepraisal' ] && [ "$(id -u)" = '0' ]; then
	find \! -user ep -exec chown ep: '{}' +
	exec su-exec ep "$@"
fi

exec "$@"
