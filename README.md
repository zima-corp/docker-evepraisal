# Docker container for Evepraisal

## Configuration

The application can be configured using environment variables or
a configuration file (evepraisal.toml).

## Environment Variables

All environment variables used by the application must be prefixed with `EVEPRAISAL_`.

For example:

- `db_path` becomes `EVEPRAISAL_DB_PATH`.
- `http_addr` becomes `EVEPRAISAL_HTTP_ADDR`.
- `base-url` becomes `EVEPRAISAL_BASE-URL`.
- `newrelic_app-name` becomes `EVEPRAISAL_NEWRELIC_APP-NAME`.

## Available Variables

| **Variable**             | **Default Value**                               | **Description**                                                           |
| ------------------------ | ----------------------------------------------- | ------------------------------------------------------------------------- |
| `ad-block`               | `""`                                            | Specifies the ad-blocking configuration.                                  |
| `backup_path`            | `"db/backups/"`                                 | Path where database backups are stored.                                   |
| `base-url`               | `"http://127.0.0.1:8080"`                       | The base URL for the application.                                         |
| `cookie-auth-key`        | _None_                                          | Key used to authenticate cookies.                                         |
| `cookie-encryption-key`  | _None_                                          | Key used for cookie encryption.                                           |
| `db_path`                | `"db/"`                                         | Path where the application database is stored.                            |
| `esi_baseurl`            | `"https://esi.evetech.net/latest"`              | Base URL for ESI (EVE Swagger Interface) API.                             |
| `extra-html-header`      | `""`                                            | Extra HTML header content to include in the application.                  |
| `extra-js`               | `""`                                            | Additional JavaScript to include in the application.                      |
| `extra-static-file-path` | `""`                                            | Path for serving extra static files.                                      |
| `http_addr`              | `":8080"`                                       | Address and port for the HTTP server.                                     |
| `http_redirect`          | `false`                                         | Redirect HTTP traffic to HTTPS if set to `true`.                          |
| `https_addr`             | `""`                                            | Address and port for the HTTPS server.                                    |
| `https_domain-whitelist` | `["evepraisal.com"]`                            | List of domains allowed for HTTPS.                                        |
| `letsencrypt_email`      | `""`                                            | Email address for Let's Encrypt notifications and certificate management. |
| `management_addr`        | `"127.0.0.1:8090"`                              | Address for the management server.                                        |
| `newrelic_app-name`      | `"Evepraisal"`                                  | Application name for New Relic monitoring.                                |
| `newrelic_license-key`   | `""`                                            | License key for New Relic integration.                                    |
| `sso-authorize-url`      | `"https://login.eveonline.com/oauth/authorize"` | URL for Single Sign-On (SSO) authorization.                               |
| `sso-client-id`          | _None_                                          | Client ID for SSO authentication.                                         |
| `sso-client-secret`      | _None_                                          | Client secret for SSO authentication.                                     |
| `sso-token-url`          | `"https://login.eveonline.com/oauth/token"`     | URL for fetching SSO tokens.                                              |
| `sso-verify-url`         | `"https://login.eveonline.com/oauth/verify"`    | URL for verifying SSO tokens.                                             |
| `market-fetch-interval`  | `30`                                            | The interval in minutes at which to fetch market data.                    |

## See

- [Docker Instructions (original)](https://github.com/evepraisal/go-evepraisal#docker-instructions-production)
