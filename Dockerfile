FROM golang:1.21-alpine AS builder

ARG ENV=prod
ARG CGO_ENABLED=1
ARG EVEPRISAL_SRC="https://github.com/evepraisal/go-evepraisal.git"
#ARG EVEPRISAL_SRC="https://github.com/AphroBytes/go-evepraisal.git"

WORKDIR "$GOPATH/src/github.com/evepraisal/go-evepraisal"

COPY . ..

RUN \
  set -eux; \
  apk upgrade --no-cache; \
  apk add --update --no-cache --virtual .build-dependencies \
  bash \
  gcc \
  git \
  make \
  musl-dev \
  ;

RUN \
  set -eux; \
  git config --global http.version HTTP/1.1; \
  git clone --depth 1 --single-branch "${EVEPRISAL_SRC}" .; \
  git apply ../patches/go-evepraisal-fetcher.patch; \
  git apply ../patches/go-evepraisal-branding.patch; \
  cp -f ../patches/favicon.ico web/resources/static/favicon.ico; \
  make setup; \
  make build; \
  make install; \
  mv "$GOPATH/bin/evepraisal" /evepraisal

FROM alpine:latest

RUN \
  set -eux; \
  apk upgrade --no-cache; \
  apk add --update --no-cache \
  curl \
  su-exec \
  tini \
  ;

COPY --from=builder /evepraisal /usr/local/bin
COPY docker-entrypoint.sh /

ARG APP_GID=10000
ARG APP_UID=10000
ARG APP_OWNER="ep"

WORKDIR /evepraisal

RUN \
  set -eux; \
  addgroup -g "${APP_GID}" "${APP_OWNER}"; \
  adduser -D -H -G "${APP_OWNER}" -u "${APP_UID}" "${APP_OWNER}"; \
  chmod +x /usr/local/bin/evepraisal; \
  mkdir db


VOLUME /evepraisal/db

EXPOSE 8080

ENTRYPOINT ["tini", "--", "/docker-entrypoint.sh"]

CMD []
